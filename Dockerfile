FROM node:carbon-slim
WORKDIR /kevin
COPY package.json yarn.lock /kevin/
RUN yarn install
COPY . /kevin
EXPOSE 8080
CMD ["yarn", "serve"]
